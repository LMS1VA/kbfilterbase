#ifndef _PUBLIC_H
#define _PUBLIC_H

/*

Change History

Modified Date: 2016-12-29
Author: LMS
Desc: Comment debug code but left here for future potential use.

Modified Date: 2016-12-03
Author: LMS
Desc: Add EnableDisable messaging through PDO.

*/


#define IOCTL_INDEX			               0x800
#define IOCTL_KBD_GETVAL_INDEX             0xF00         // per ntddk.h   2048-4095 are reserved for customers. I'm just picking a high arbiterary number
// #define IOCTL_KBD_SETVAL_INDEX             0xF01
#define IOCTL_KBD_SETVAL_INDEX_CADOn       0xF10
#define IOCTL_KBD_SETVAL_INDEX_CADOff      0xF11

#define IOCTL_KBFILTR_GET_KEYBOARD_ATTRIBUTES	CTL_CODE( FILE_DEVICE_KEYBOARD,   \
                                                        IOCTL_INDEX,    \
                                                        METHOD_BUFFERED,    \
                                                        FILE_READ_DATA)

#define IOCTL_KBFILTR_GET_KB_DRIVER_VALUES		CTL_CODE( FILE_DEVICE_KEYBOARD,   \
                                                        IOCTL_KBD_GETVAL_INDEX,    \
                                                        METHOD_BUFFERED,    \
                                                        FILE_READ_DATA)

/*
#define IOCTL_KBFILTR_SET_KB_DRIVER_VALUES		CTL_CODE( FILE_DEVICE_KEYBOARD,   \
                                                        IOCTL_KBD_SETVAL_INDEX,    \
                                                        METHOD_BUFFERED,    \
                                                        FILE_WRITE_DATA)
*/

#define IOCTL_KBFILTR_SET_KB_DRIVER_VALUES_CADOn		CTL_CODE( FILE_DEVICE_KEYBOARD,   \
																IOCTL_KBD_SETVAL_INDEX_CADOn,    \
																METHOD_BUFFERED,    \
																FILE_WRITE_DATA)

#define IOCTL_KBFILTR_SET_KB_DRIVER_VALUES_CADOff		CTL_CODE( FILE_DEVICE_KEYBOARD,   \
																IOCTL_KBD_SETVAL_INDEX_CADOff,    \
																METHOD_BUFFERED,    \
																FILE_WRITE_DATA)


#endif
